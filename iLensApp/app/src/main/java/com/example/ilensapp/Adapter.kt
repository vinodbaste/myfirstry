package com.example.ilensapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.text.DateFormat

 class Adapter (val items :List<items>) :
    RecyclerView.Adapter<Adapter.ItemViewHolder>() {
    class ItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val image = itemView.findViewById<ImageView>(R.id.Image)
        val title = itemView.findViewById<TextView>(R.id.title)
        val date = itemView.findViewById<TextView>(R.id.date)
    }

     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
         val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_recent_apps,parent,false)
         return ItemViewHolder(view)
     }

     override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.title.text = items[position].title
         holder.date.text = items[position].date
     }

     override fun getItemCount(): Int {
         return items.size
     }
 }